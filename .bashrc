
# _		  _     
#| |		 | |
#| |__   __ _ ___| |__  _ __ ___ 
#| '_ \ / _` / __| '_ \| '__/ __|
#| |_) | (_| \__ \ | | | | | (__ 
#|_.__/ \__,_|___/_| |_|_|  \___|
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Setup pretty colors
# [ -n "$XTERM_VERSION" ]      && transset-df --id "$WINDOWID" 0.9 >/dev/null
[ -e ~/.cache/wal/sequences ] && cat ~/.cache/wal/sequences

# Make cd print ls
function cd {
    builtin cd "$@" && ls --color=auto --group-directories-first 
}

# Setup aliases
alias grep="grep --color=auto"
alias ls='ls --color=auto --group-directories-first'
alias la="ls -a"
alias ll="ls -l"
alias llh="ls -lh"
alias llah="ls -lah"

# Bash PS1
# export PS1="\e[0;31m\u\e[0m@\e[0;34m\H \e[1;33m\W\e[0m \\$\[$(tput sgr0)\] "
export PS1="\e[0;31m\u\e[0m@\e[0;34m\`cat /etc/hostname\` \e[1;33m\W\e[0m \\$\[$(tput sgr0)\] "


# ZSH PS1
if [[ $UID == 0 ]]
then
	PROMPT='%F{red}%n%f@%F{blue}%m%f %F{yellow}%1~%f # '
	RPROMPT='[%F{yellow}%?%f]'
else
	PROMPT='%F{red}%n%f@%F{blue}%m%f %F{yellow}%1~%f $ '
	RPROMPT='[%F{yellow}%?%f]'
fi


#alias c="cat ~/.cache/wal/sequences"
