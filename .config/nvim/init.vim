
call plug#begin('~/.local/share/nvim/plugged')

Plug 'junegunn/vim-easy-align'
"Plug 'lervag/vimtex'
Plug 'mboughaba/i3config.vim'
Plug 'dylanaraps/wal.vim'

call plug#end()

syntax on
filetype plugin indent on

let g:tex_flavor = 'latex'

let g:maplocalleader = "\\"

autocmd!
autocmd BufWritePost ~/.config/polybar/* :!systemctl restart --user polybar
autocmd BufWritePost ~/.config/i3/config :!i3-msg reload

"Relative line numbering
set nu rnu

" Automatically highlight syntax
syntax on
" Add colors
colorscheme wal

" Use clipboard
set clipboard=unnamedplus
