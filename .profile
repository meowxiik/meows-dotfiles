#!/bin/sh

export EDITOR="nvim"
export VISUAL="nvim"
export TERMINAL="kitty"
export BROWSER="qutebrowser"
export TRUEBROWSER="qutebrowser"
export READER="zathura"
export QT_QPA_PLATFORMTHEME="qt5ct"

export XMODIFIERS=@im=uim
export GTK_IM_MODULE=uim
export QT_IM_MODULE=uim

PATH=/home/meowxiik/.scripts:$PATH
export PATH
